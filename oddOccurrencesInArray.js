function solution(A){

    let oddElem = 0;

    for (let i = 0; i < A.length; i++)
    {
        oddElem ^= A[i];
    }

    return oddElem;
}

let A = [9, 3, 9, 3, 9, 7, 9];
console.log(solution(A));